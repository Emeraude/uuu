#!/usr/bin/env sh

[ $(whoami) != "root" ] && echo "Warning: this script should be ran as root." >&2

# set_sessions_lock_status ACTION USER [SESSION...]
set_sessions_lock_status() {
  action="$1"
  user="$2"
  shift 2

  if [ "$action" != "lock" ] && [ "$action" != "unlock" ]; then
    echo "Invalid action `$action`."' Should be either "lock" or "unlock".'
    false
    return
  fi
  echo "$action sessions $@ for user $user"
  sudo -u "$user" loginctl "$action"-session $@
}

# get_sessions_for_user USER
get_sessions_for_user() {
  sudo -u "$1" loginctl list-sessions --no-legend | sed -E 's/^\s*([0-9]+).*$/\1/'
}

# set_all_sessions_lock_status_for_user ACTION USER
set_all_sessions_lock_status_for_user() {
  sessions=$(get_sessions_for_user "$2")
  set_sessions_lock_status "$1" "$2" $sessions
}

# get_users_with_active_sessions
get_users_with_active_sessions() {
  loginctl list-sessions --no-pager --no-legend | cut -d' ' -f3 | sort | uniq
}

# get_active_users_and_their_config_dirs
get_active_users_and_their_config_dirs() {
  for user in $(getent passwd | cut -d':' -f1); do
    get_users_with_active_sessions | grep -wq "$user" || continue
    configdir=$(sudo -iu "$user" echo '$XDG_CONFIG_HOME/uuu/' 2>/dev/null)
    [ $? -ne 0 ] && continue

    if [ $? -ne 0 ] && [ -z "$configdir" ]; then
      configdir=$(sudo -iu "$user" echo '$HOME/.config/uuu/' 2>/dev/null)
      [ $? -ne 0 ] && continue
    fi
    echo "$user:$configdir"
  done
}

# interpret_configuration_file ACTION USER PATH
interpret_configuration_file() {
  if [ -r "$3" ]; then
    sed -E -e 's/(\s*#.*$|^\s+|\s+$)//g' -e '/^$/d' -- "$3"  |\
      while IFS="" read -r id || [ -n "$id" ]
      do
        if [ "$id" = "$ID_SERIAL" ]; then
          set_all_sessions_lock_status_for_user "$1" "$2"
        fi
      done
  fi
}

for i in $(get_active_users_and_their_config_dirs); do
  user=$(echo $i | cut -d':' -f1)
  configdir=$(echo $i | cut -d':' -f2)

  if [ "$ACTION" = "add" ]; then
    interpret_configuration_file unlock "$user" "$configdir"/unlock.conf
  elif [ "$ACTION" = "remove" ]; then
    interpret_configuration_file lock "$user" "$configdir"/lock.conf
  fi
done
