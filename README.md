# Usb Udev Unlocker

A tool to unlock or lock user sessions upon addition or removal of a USB device using udev. It relies on systemd to handle the sessions and udev to handle the USB devices.

## Requirements

This script requires `sh`, `sudo`, `systemd`, `udev` and `getent`.

## Installation

```sh
cp uuu.sh /sbin/uuu
cp default.rules /etc/udev/rules.d/75-uuu.rules
mkdir $XDG_CONFIG_HOME/uuu
$EDITOR $XDG_CONFIG_HOME/uuu/unlock.conf
chmod 0600 $XDG_CONFIG_HOME/uuu/unlock.conf
```

IDs of devices can be retrieved by looking at the content of `/dev/disk/by-id` or with `udevadm info --name=DEVICENAME | grep ID_SERIAL=`.

## Security

As this software provides another way to unlock your current session, it decreases the computer security de facto.  
It relies on the ID of USB devices plugged in the computer. Bear in mind that even if these IDs are supposed to be unique, it is still possible for an attacker to duplicate them and make a perfect copy of the USB device, which is not something we are capable of detecting as of now.  
Since configuration files present in `$XDG_CONFIG_HOME/uuu` contain sensible data (IDs of devices used to unlock the computer) their permission should be reduced to their minimum, aka `0600` (or even `0000` if you're super paranoid).

Please bear in mind that the `lock` feature is not sufficient to prevent the user from using the computer with the USB device unplugged. It is still possible to login to the system or to unlock it through other means.

To reduce its attack surface, the dependencies have been reduced to their minimum (planning on making `sudo` and `getent` optional in the future).  

This software has not been audited and probably never will be. However, if you find any vulnerability or have any concern feel free to open an issue describing it.

## What about the name?

lack of inspiration.
